package com.karma.bgmikdcalculator.customization

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.customization.bacpack.BackPackModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.gson.Gson
import com.karma.bgmikdcalculator.R

private lateinit var linBagTable: LinearLayout
private lateinit var linHead: LinearLayout
private lateinit var tvTittle: TextView
private lateinit var frEmpty: FrameLayout
private lateinit var tvbag: TextView
private lateinit var imgbag: ImageView
private lateinit var relheading: RelativeLayout
lateinit var mAdView1: AdView

class SaveDataActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_data)
        linBagTable = findViewById(R.id.linBagTable)
        frEmpty = findViewById(R.id.frEmpty)
        linHead = findViewById(R.id.linHead)
        tvTittle = findViewById(R.id.tvTittle)
        tvbag = findViewById(R.id.tvbag)
        imgbag = findViewById(R.id.imgbag)
        relheading = findViewById(R.id.relheading)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
       mAdView1.loadAd(adRequest1)


        linHead.visibility = View.INVISIBLE
        relheading.visibility = View.GONE
        frEmpty.visibility = View.VISIBLE
        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences("karmaCustmizationPref", Context.MODE_PRIVATE)

        val levelBag = intent.extras?.getString("BagLevel")
        if (levelBag != null) {
            tvbag.setText(levelBag)
            if (levelBag.equals("Level 1 bag")) {
                imgbag!!.setBackgroundResource(R.drawable.level_one_bag)
            } else if (levelBag.equals("Level 2 bag")) {
                imgbag!!.setBackgroundResource(R.drawable.level_two_bag)
            } else if (levelBag.equals("Level 3 bag")) {
                imgbag!!.setBackgroundResource(R.drawable.level_three_bag)
            }


            val json = sharedPreferences.getString(levelBag, "")

            if (!json!!.isEmpty()) {
                val gson = Gson()

                val bagPack: BackPackModel = gson.fromJson(json, BackPackModel::class.java)
                tvTittle.text =
                    "Total " + ("%.2f".format(bagPack.filledCount)) + "/" + bagPack.totalCount

                for (eachItem in bagPack.listItems) {

                    if (eachItem.itemCount > 0) {
                        val inflater =
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val rowView: View = inflater.inflate(R.layout.bag_table, null)
                        val img = rowView.findViewById<ImageView>(R.id.img)
                        val tvItemName = rowView.findViewById<TextView>(R.id.tvItemName)
                        val tvcapacity = rowView.findViewById<TextView>(R.id.tvcapacity)
                        val tvCount = rowView.findViewById<TextView>(R.id.tvCount)

                        img.setImageResource(eachItem.image)
                        tvItemName.setText(eachItem.name)
                        tvcapacity.setText("" + eachItem.capacity)
                        tvCount.setText("" + eachItem.itemCount)
                        linHead.visibility = View.VISIBLE
                        relheading.visibility = View.VISIBLE
                        frEmpty.visibility = View.GONE
                        linBagTable!!.addView(rowView)
                    }
                }
            }
        }

    }


}