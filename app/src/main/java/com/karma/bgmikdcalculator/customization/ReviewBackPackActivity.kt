package com.karma.bgmikdcalculator.customization

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.customization.bacpack.BackPackItem
import com.example.homeactivity.customization.bacpack.BackPackModel
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.gson.Gson
import com.karma.bgmikdcalculator.R
import java.util.*

private lateinit var tvTittle: TextView
private lateinit var linBagTable: LinearLayout
private lateinit var linHead: LinearLayout
private lateinit var btnSave: Button
private var selectedList = ArrayList<BackPackItem>()
private var bagLevel: String? = ""
private var bagPack: BackPackModel? = null
lateinit var mAdView2: AdView
private var mInterstitialAd: InterstitialAd? = null

class ReviewBackPackActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_back_pack)
        tvTittle = findViewById(R.id.tvTittle)
        linBagTable = findViewById(R.id.linBagTable)
        linHead = findViewById(R.id.linHead)
        btnSave = findViewById(R.id.btnSave)
        linHead.visibility = View.INVISIBLE
        mAdView2 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView2.loadAd(adRequest1)

        bagLevel = intent.getStringExtra("bagLevel")
        bagPack = intent.getParcelableExtra<BackPackModel>("bagpack")
        val list = intent.getParcelableArrayListExtra<BackPackItem>("list")

        tvTittle.text =
            "Total " + ("%.2f".format(bagPack!!.filledCount)) + "/" + bagPack!!.totalCount
        selectedList.clear()
        linBagTable.removeAllViews()
        if (list != null) {
            for (eachItem in list) {

                if (eachItem.itemCount > 0) {
                    selectedList.add(eachItem)
                    val inflater =
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val rowView: View = inflater.inflate(R.layout.bag_table, null)
                    val img = rowView.findViewById<ImageView>(R.id.img)
                    val tvItemName = rowView.findViewById<TextView>(R.id.tvItemName)
                    val tvcapacity = rowView.findViewById<TextView>(R.id.tvcapacity)
                    val tvCount = rowView.findViewById<TextView>(R.id.tvCount)

                    img.setImageResource(eachItem.image)
                    tvItemName.setText(eachItem.name)
                    tvcapacity.setText("" + eachItem.capacity)
                    tvCount.setText("" + eachItem.itemCount)
                    linHead.visibility = View.VISIBLE
                    linBagTable!!.addView(rowView)
                }
            }
        }
        bagPack!!.listItems = selectedList

        btnSave.setOnClickListener {

            if (mInterstitialAd != null) {
                showCallback()
                mInterstitialAd?.show(this)
                Log.d("TAG", "SHOW")

            } else {
                save()
            }

        }



    }

    private fun showCallback() {
        mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                Log.d("TAG", "Ad was dismissed.")
                save()
            }

            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                Log.d("TAG", "Ad failed to show.")
                save()
            }

            override fun onAdShowedFullScreenContent() {
                Log.d("TAG", "Ad showed fullscreen content.")
                mInterstitialAd = null
            }
        }
    }

    private fun save() {

        var gson = Gson()
        var jsonString = gson.toJson(bagPack)
        Log.d("BAG", "JSON " + jsonString)


        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences("karmaCustmizationPref", Context.MODE_PRIVATE)

        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(bagLevel, jsonString)
        editor.apply()
        editor.commit()
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()
        var adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            getString(R.string.add_mob_interstitial_id),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d("TAG", "fail to load" + adError?.message)
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d("TAG", "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                }
            })
    }

}