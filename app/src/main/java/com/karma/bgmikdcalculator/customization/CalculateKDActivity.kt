package com.example.homeactivity.customization

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.karma.bgmikdcalculator.R

private lateinit var tvKills: TextView
private lateinit var etKD: EditText
private lateinit var etCMP: EditText
private lateinit var etCK: EditText
private lateinit var tvCKD: TextView
private lateinit var tvSinglematch: TextView
private lateinit var tvNoOfMatchTobePlayed: TextView
private lateinit var linScrol: LinearLayout


private lateinit var thumbView: View
private lateinit var newKDCard: CardView
private var seekValue: Float = 1.0f
lateinit var mAdView1: AdView

class CalculateKDActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculate_kd)
        MobileAds.initialize(this) {}
        linScrol = findViewById(R.id.linScrol)
        etCMP = findViewById(R.id.etCMP)
        etCK = findViewById(R.id.etCK)
        tvCKD = findViewById(R.id.tvCKD)
        tvSinglematch = findViewById(R.id.tvSinglematch)
        newKDCard = findViewById(R.id.newKDCard)
        tvNoOfMatchTobePlayed = findViewById(R.id.tvNoOfMatchTobePlayed)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)

        tvKills = findViewById(R.id.tvKills)
        etKD = findViewById(R.id.etKD)
        val adRequest = AdRequest.Builder().build()

        thumbView = LayoutInflater.from(this)
            .inflate(R.layout.layout_seekbar_thumb, null, false)

        newKDCard.visibility = View.INVISIBLE
        val sk = findViewById<SeekBar>(R.id.sbNoOFMatch)
        sk.thumb = getThumb(1)
        tvNoOfMatchTobePlayed.setText("Slide right to change no of games you will play 1")



        sk.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, b: Boolean) {
                seekBar.thumb = getThumb(progress)
                if (etKD.text.isEmpty()) {

                    etKD.setError("Cannot be empty")
                } else {
                    if (progress < 1) {
                        /* if seek bar value is lesser than min value then set min value to seek bar */
                        seekBar.setProgress(1)
                        seekValue = 1.0f
                        tvNoOfMatchTobePlayed.setText("Slide right to change no of games you will play  1")
                    } else {

                        seekValue = progress.toFloat()
                        tvNoOfMatchTobePlayed.setText("Slide right to change no of games you will play " + progress)

                    }
                    calculateNewKD()
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        etCK.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                if (s != null && !s.isEmpty()) {

                    try {

                        val noOfCMatch = (etCMP.text.toString()).toFloat()
                        val noOfCKills = (etCK.text.toString()).toFloat()
                        tvCKD.text =
                            "Current KD is " + "%.2f".format((noOfCKills.toFloat() / noOfCMatch.toFloat()))

                        newKDCard.visibility = View.VISIBLE

                    } catch (e: Exception) {
                        e.printStackTrace()
                        tvCKD.text = "Enter valid digit"
                    }
                } else {
                    try {
                        //clear all values of New KD card
                        newKDCard.visibility = View.INVISIBLE
                        sk.setProgress(1)
                        tvCKD.text = ""
                        etKD.setText("")
                        tvKills.text = ""
                        tvSinglematch.text = ""
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }
        })
        etCMP.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                etCK.setText("")

            }
        })
        etKD.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                if (s != null && !s.isEmpty()) {

                    try {
                        if (etKD.text.toString()
                                .toFloat() <= ((etCK.text.toString()).toFloat() / (etCMP.text.toString()).toFloat())
                        ) {
                            tvKills.text = "New KD should be greater then previous KD"
                            tvSinglematch.text = ""
                            etKD.setError("less then prev KD")
                        } else
                            calculateNewKD()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }
        })

    }

    private fun calculateNewKD() {

        val noOfCMatch = (etCMP.text.toString()).toFloat()
        val noOfCKills = (etCK.text.toString()).toFloat()
        val newKd = (etKD.text.toString()).toFloat()
        val newKill = ((noOfCMatch + seekValue) * newKd) - noOfCKills
        val newVErificationKD =
            (noOfCKills + newKill).toFloat() / (noOfCMatch + seekValue).toFloat()
        tvKills.text =
            "In next $seekValue  matches you have to kill $newKill to make your KD $newKd"
        tvSinglematch.setText("" + "%.2f".format((newKill.toFloat() / seekValue.toFloat())))

    }


    fun getThumb(progress: Int): Drawable? {
        (thumbView.findViewById(R.id.tvProgress) as TextView).text = progress.toString() + ""
        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap = Bitmap.createBitmap(
            thumbView.measuredWidth,
            thumbView.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        thumbView.layout(0, 0, thumbView.measuredWidth, thumbView.measuredHeight)
        thumbView.draw(canvas)
        return BitmapDrawable(resources, bitmap)
    }
}