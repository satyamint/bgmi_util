package com.example.homeactivity.customization.bacpack

import android.os.Parcel
import android.os.Parcelable


class BackPackModel() : Parcelable {
    var totalCount: Float = 0f
    var filledCount: Float = 0f
    var listItems = ArrayList<BackPackItem>()

    constructor(parcel: Parcel) : this() {
        totalCount = parcel.readFloat()
        filledCount = parcel.readFloat()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(totalCount)
        parcel.writeFloat(filledCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BackPackModel> {
        override fun createFromParcel(parcel: Parcel): BackPackModel {
            return BackPackModel(parcel)
        }

        override fun newArray(size: Int): Array<BackPackModel?> {
            return arrayOfNulls(size)
        }
    }
}

class BackPackItem() : Parcelable{
    var name: String? = null
    var image: Int = 0
    var itemCount: Int = 0
    var capacity: Float = 0f

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        image = parcel.readInt()
        itemCount = parcel.readInt()
        capacity = parcel.readFloat()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(image)
        parcel.writeInt(itemCount)
        parcel.writeFloat(capacity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BackPackItem> {
        override fun createFromParcel(parcel: Parcel): BackPackItem {
            return BackPackItem(parcel)
        }

        override fun newArray(size: Int): Array<BackPackItem?> {
            return arrayOfNulls(size)
        }
    }
}
