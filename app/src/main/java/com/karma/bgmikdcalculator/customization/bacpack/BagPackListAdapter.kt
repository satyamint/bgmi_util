package com.example.homeactivity.customization.bacpack


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.karma.bgmikdcalculator.R
import java.util.*

class BagPackListAdapter(
    private val activity: BagPackActivity,
    val listener: BagPackActivity
) :
    RecyclerView.Adapter<BagPackListAdapter.ViewHolder>() {
    public var backPackItem = BackPackModel()


    class ViewHolder(itemView: View, ViewType: Int) : RecyclerView.ViewHolder(itemView) {


        internal var imgItem: ImageView = itemView.findViewById(R.id.imgItem)
        internal var tvItemName: TextView = itemView.findViewById(R.id.tvItemName)
        internal var tvItemCapacity: TextView = itemView.findViewById(R.id.tvItemCapacity)
        internal var tvCountEachItem: TextView = itemView.findViewById(R.id.tvCountEachItem)
        internal var linMinus: LinearLayout = itemView.findViewById(R.id.linMinus)
        internal var linPlus: LinearLayout = itemView.findViewById(R.id.linPlus)
        internal var btnAdd: Button = itemView.findViewById(R.id.btnAdd)
        internal var btnRemove: Button = itemView.findViewById(R.id.btnRemove)


    }

    override fun onBindViewHolder(holder: ViewHolder, posItem: Int) {
        holder.tvItemName.text = backPackItem.listItems.get(posItem).name
        holder.imgItem.setImageResource(backPackItem.listItems.get(posItem).image)
        holder.tvItemCapacity.text = "" + backPackItem.listItems.get(posItem).capacity + " * " + backPackItem.listItems.get(holder.adapterPosition).itemCount
        holder.linMinus.setOnClickListener {
            var count = ((Integer.parseInt(holder.tvCountEachItem.text.toString())) - 1)
            if (count < 0) {
                count = 0
            }
            holder.tvCountEachItem.text = "" + count

        }
        holder.btnRemove.setOnClickListener {
            val tvCount = (Integer.parseInt(holder.tvCountEachItem.text.toString()))
            if (backPackItem.listItems.get(holder.adapterPosition).itemCount > 0) {
                if (backPackItem.listItems.get(holder.adapterPosition).itemCount >= tvCount
                ) {

                    backPackItem.listItems.get(holder.adapterPosition).itemCount =
                        backPackItem.listItems.get(holder.adapterPosition).itemCount - tvCount
                    backPackItem.filledCount =
                        backPackItem.filledCount - (backPackItem.listItems.get(holder.adapterPosition).capacity * tvCount)
                    listener.onItemClick(
                        backPackItem,
                        holder.btnRemove,
                        posItem
                    )
                    holder.tvItemCapacity.text = "" + backPackItem.listItems.get(posItem).capacity + " * " +  backPackItem.listItems.get(holder.adapterPosition).itemCount

                } else {
                    Toast.makeText(
                        activity,
                        "Enter value should be less or equal to ${backPackItem.listItems.get(holder.adapterPosition).itemCount} ",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            } else {
                Toast.makeText(
                    activity,
                    "This item is not added so cannot be removed ",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        holder.btnAdd.setOnClickListener {

            val tvCount = (Integer.parseInt(holder.tvCountEachItem.text.toString()))
            if (((backPackItem.listItems.get(holder.adapterPosition).capacity * tvCount)
                        + backPackItem.filledCount)
                <= backPackItem.totalCount
            ) {
                backPackItem.listItems.get(holder.adapterPosition).itemCount =
                    backPackItem.listItems.get(holder.adapterPosition).itemCount + tvCount
                backPackItem.filledCount =
                    backPackItem.filledCount +
                            (backPackItem.listItems.get(holder.adapterPosition).capacity * tvCount)
                listener.onItemClick(
                    backPackItem,
                    holder.btnAdd,
                    posItem
                )
                holder.tvItemCapacity.text = "" + backPackItem.listItems.get(posItem).capacity + " * " +  backPackItem.listItems.get(holder.adapterPosition).itemCount

            } else {
                Toast.makeText(activity, "Bag is full ", Toast.LENGTH_SHORT).show()
            }
        }

        holder.linPlus.setOnClickListener {

            var count = ((Integer.parseInt(holder.tvCountEachItem.text.toString())) + 1)
            holder.tvCountEachItem.text =
                "" + count


        }
        holder.tvCountEachItem.text ="0"

    }

    private fun isPresentInbag(item: BackPackItem, listItems: ArrayList<BackPackItem>): Boolean {


        for (eactItem in listItems) {

            if (eactItem.name.equals(item.name)) {
                return true
            }

        }
        return false
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.bac_pack_item, parent, false) //Inflating the layout
        return ViewHolder(v, viewType)
    }

    override fun getItemCount(): Int {
        return backPackItem.listItems.size

    }

    fun setList(backPack: BackPackModel) {
        backPackItem = backPack
    }


}