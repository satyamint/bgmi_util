package com.example.homeactivity.customization.bacpack

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homeactivity.callbacks.OnItemClickListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.karma.bgmikdcalculator.R
import com.karma.bgmikdcalculator.customization.ReviewBackPackActivity
import java.util.*

private var listItems = ArrayList<BackPackItem>()
private var backPackItem = BackPackModel()
private var adapter: BagPackListAdapter? = null
private var listRecycleView: RecyclerView? = null
private var tvcapacity: TextView? = null
private var tvbag: TextView? = null
private var imgbag: ImageView? = null
private var btnShowPreview: Button? = null
lateinit var mAdView1: AdView
class BagPackActivity : AppCompatActivity(), OnItemClickListener<BackPackModel> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bag_pack)
        MobileAds.initialize(this) {}
        listRecycleView = findViewById<View>(R.id.recycleview) as RecyclerView
        tvcapacity = findViewById<View>(R.id.tvcapacity) as TextView
        tvbag = findViewById<View>(R.id.tvbag) as TextView
        imgbag = findViewById<View>(R.id.imgbag) as ImageView
        btnShowPreview = findViewById<View>(R.id.btnShowPreview) as Button
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)

//        no bag 150
//        level 1 300
//        level 2 350
//        level 3 400

        val levelBag = intent.extras?.getString("BagLevel")

        listRecycleView?.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 2)
        listRecycleView?.layoutManager = layoutManager
        adapter = BagPackListAdapter(this@BagPackActivity, this)
        listRecycleView?.adapter = adapter

        getValues()
        if (levelBag.equals("1")) {
            backPackItem.totalCount = 300f
            tvbag!!.setText("Level 1 bag")
            imgbag!!.setBackgroundResource(R.drawable.level_one_bag)
        } else if (levelBag.equals("2")) {
            backPackItem.totalCount = 350f
            tvbag!!.setText("Level 2 bag")
            imgbag!!.setBackgroundResource(R.drawable.level_two_bag)
        } else if (levelBag.equals("3")) {
            backPackItem.totalCount = 400f
            tvbag!!.setText("Level 3 bag")
            imgbag!!.setBackgroundResource(R.drawable.level_three_bag)
        } else {
            tvbag!!.setText("Empty")
            backPackItem.totalCount = 150f
        }

        backPackItem.filledCount = 0f
        backPackItem.listItems = listItems

        tvcapacity!!.setText("${"%.2f".format(backPackItem.filledCount)} / ${backPackItem.totalCount}")

        adapter!!.setList(backPackItem)
        btnShowPreview!!.setOnClickListener {

            val intent = Intent(this@BagPackActivity, ReviewBackPackActivity::class.java)
            intent.putExtra("bagLevel", tvbag!!.text.toString())
            intent.putExtra("bagpack", adapter!!.backPackItem)
            intent.putParcelableArrayListExtra("list", adapter!!.backPackItem.listItems)
            startActivity(intent)

        }

    }

    override fun onItemClick(backPackModel: BackPackModel, view: View, position: Int) {

        tvcapacity!!.setText("${"%.2f".format(backPackModel.filledCount)} / ${backPackModel.totalCount}")
        if (view.getTag().toString().equals("PLUS")) {

        } else {

        }

    }

    private fun getValues() {

        listItems.clear()

        val one = BackPackItem()
        one.name = "5.56 MM"
        one.image = R.drawable.fivefivesix_mm
        one.capacity = 0.5f
        one.itemCount = 0
        listItems.add(one)

        val one1 = BackPackItem()
        one1.name = "7.62 MM"
        one1.image = R.drawable.sevensixtwo_mm
        one1.capacity = 0.60f
        one1.itemCount = 0
        listItems.add(one1)

        val one2 = BackPackItem()
        one2.name = "9 MM"
        one2.image = R.drawable.nine_mm
        one2.capacity = 0.38f
        one2.itemCount = 0
        listItems.add(one2)

        val one3 = BackPackItem()
        one3.name = ".45 Acp"
        one3.image = R.drawable.fourtyfive_acp
        one3.capacity = 0.4f
        one3.itemCount = 0
        listItems.add(one3)

        val one4 = BackPackItem()
        one4.name = "300 Magmun"
        one4.image = R.drawable.threehundred_magnum
        one4.capacity = 1f
        one4.itemCount = 0
        listItems.add(one4)

        val one5 = BackPackItem()
        one5.name = "12 gauge"
        one5.image = R.drawable.twelve_gauge
        one5.capacity = 1.25f
        one5.itemCount = 0
        listItems.add(one5)

        val one6 = BackPackItem()
        one6.name = "Bolt Crossbow"
        one6.image = R.drawable.bolt_crossbow
        one6.capacity = 2f
        one6.itemCount = 0
        listItems.add(one6)

        val one7 = BackPackItem()
        one7.name = "Frag Grenade"
        one7.image = R.drawable.grainade
        one7.capacity = 18f
        one7.itemCount = 0
        listItems.add(one7)

        val one8 = BackPackItem()
        one8.name = "Molotov Cocktail"
        one8.image = R.drawable.molatal
        one8.capacity = 16f
        one8.itemCount = 0
        listItems.add(one8)

        val one9 = BackPackItem()
        one9.name = "Smoke Grenade"
        one9.image = R.drawable.smoke
        one9.capacity = 14f
        one9.itemCount = 0
        listItems.add(one9)

        val two = BackPackItem()
        two.name = "Stun Grenade"
        two.image = R.drawable.stunt
        two.capacity = 12f
        two.itemCount = 0
        listItems.add(two)

        val two1 = BackPackItem()
        two1.name = "Adrenaline Syringe"
        two1.image = R.drawable.adrilin
        two1.capacity = 20f
        two1.itemCount = 0
        listItems.add(two1)

        val two2 = BackPackItem()
        two2.name = "Bandage"
        two2.image = R.drawable.bandage
        two2.capacity = 2f
        two2.itemCount = 0
        listItems.add(two2)

        val two3 = BackPackItem()
        two3.name = "Energy Drink"
        two3.image = R.drawable.drink
        two3.capacity = 4f
        two3.itemCount = 0
        listItems.add(two3)

        val two4 = BackPackItem()
        two4.name = "First Aid Kit"
        two4.image = R.drawable.first_aid
        two4.capacity = 10f
        two4.itemCount = 0
        listItems.add(two4)

        val two5 = BackPackItem()
        two5.name = "Gas Can"
        two5.image = R.drawable.gascan
        two5.capacity = 20f
        two5.itemCount = 0
        listItems.add(two5)

        val two6 = BackPackItem()
        two6.name = "Med Kit"
        two6.image = R.drawable.medkit
        two6.capacity = 20f
        two6.itemCount = 0
        listItems.add(two6)

        val two7 = BackPackItem()
        two7.name = "Painkiller"
        two7.image = R.drawable.painkiller
        two7.capacity = 10f
        two7.itemCount = 0
        listItems.add(two7)

    }


}