package com.karma.bgmikdcalculator

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.homeactivity.customization.CalculateKDActivity
import com.example.homeactivity.customization.bacpack.BagPackActivity
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.karma.bgmikdcalculator.customization.SaveDataActivity


private lateinit var btnKd: Button
private lateinit var btnBagPack: Button
private lateinit var btnSaved: Button
private var isClicked = false
private var clickItem = ""

lateinit var mAdView1: AdView
private var mInterstitialAd: InterstitialAd? = null

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this) {}
        btnKd = findViewById(R.id.btnKd)
        btnBagPack = findViewById(R.id.btnBagPack)
        btnSaved = findViewById(R.id.btnSaved)
        mAdView1 = findViewById(R.id.adView1)
        val adRequest1 = AdRequest.Builder().build()
        mAdView1.loadAd(adRequest1)
        btnKd.setOnClickListener {


                val intent = Intent(this@MainActivity, CalculateKDActivity::class.java)
                startActivity(intent)

        }

        btnBagPack.setOnClickListener {

                openDilog("bagpack")

        }

        btnSaved.setOnClickListener {
            if (mInterstitialAd != null) {
                clickItem = "saved"
                showCallback()
                mInterstitialAd?.show(this)
                Log.d("TAG", "SHOW")

            } else {
                openDilog("saved")
            }
        }


    }

    private fun showCallback() {
        mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                Log.d("TAG", "Ad was dismissed.")
                openDilog(clickItem)
            }

            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                Log.d("TAG", "Ad failed to show.")
                openDilog(clickItem)
            }

            override fun onAdShowedFullScreenContent() {
                Log.d("TAG", "Ad showed fullscreen content.")
                mInterstitialAd = null
            }
        }
    }

    var alertDialog: AlertDialog? = null
    private fun openDilog(str: String) {
        if (str.isEmpty())
            return

        clickItem = ""
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater: LayoutInflater = this.getLayoutInflater()
        val dialogView: View = inflater.inflate(R.layout.create_bagpop_up, null)
        dialogBuilder.setView(dialogView)

        val relone = dialogView.findViewById<View>(R.id.relone) as RelativeLayout
        val reltwo = dialogView.findViewById<View>(R.id.reltwo) as RelativeLayout
        val relthree = dialogView.findViewById<View>(R.id.relthree) as RelativeLayout
        val relNoBag = dialogView.findViewById<View>(R.id.relNoBag) as RelativeLayout


        relone.setOnClickListener {
            alertDialog?.dismiss()

            if (str.equals("saved")) {
                val intent = Intent(this@MainActivity, SaveDataActivity::class.java)
                intent.putExtra("BagLevel", "Level 1 bag")
                startActivity(intent)
            } else {
                val intent = Intent(this@MainActivity, BagPackActivity::class.java)
                intent.putExtra("BagLevel", "1")
                startActivity(intent)
            }
        }
        reltwo.setOnClickListener {
            alertDialog?.dismiss()
            if (str.equals("saved")) {
                val intent = Intent(this@MainActivity, SaveDataActivity::class.java)
                intent.putExtra("BagLevel", "Level 2 bag")
                startActivity(intent)
            } else {
                val intent = Intent(this@MainActivity, BagPackActivity::class.java)
                intent.putExtra("BagLevel", "2")
                startActivity(intent)
            }
        }
        relthree.setOnClickListener {
            alertDialog?.dismiss()
            if (str.equals("saved")) {
                val intent = Intent(this@MainActivity, SaveDataActivity::class.java)
                intent.putExtra("BagLevel", "Level 3 bag")
                startActivity(intent)
            } else {
                val intent = Intent(this@MainActivity, BagPackActivity::class.java)
                intent.putExtra("BagLevel", "3")
                startActivity(intent)
            }
        }
        relNoBag.setOnClickListener {
            alertDialog?.dismiss()
            if (str.equals("saved")) {
                val intent = Intent(this@MainActivity, SaveDataActivity::class.java)
                intent.putExtra("BagLevel", "Empty")
                startActivity(intent)
            } else {
                val intent = Intent(this@MainActivity, BagPackActivity::class.java)
                intent.putExtra("BagLevel", "0")
                startActivity(intent)
            }
        }
        alertDialog = dialogBuilder.create()
        alertDialog?.show()
    }

    override fun onStart() {
        super.onStart()
        var adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            getString(R.string.add_mob_interstitial_id),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d("TAG", "fail to load" + adError?.message)
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d("TAG", "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                }
            })
    }

}